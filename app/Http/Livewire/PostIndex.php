<?php

namespace App\Http\Livewire;

use App\Post;
use Livewire\Component;
use Livewire\WithPagination;

class PostIndex extends Component
{
    use WithPagination;

//    public $postAdded = false;

    protected $listeners = [
        'postAdded'
    ];

    public function postAdded($postId) {
//        $this->postAdded = true;
        session()->flash('message', 'Your post was added');
    }

    public function mount() {
        $this->posts = Post::latest()->get();
    }

    public function render()
    {
        return view('livewire.post-index', [
            'posts' => Post::latest()->paginate(3)
        ]);
    }
}
