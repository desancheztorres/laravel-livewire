<div>
    <div class="mb-4">
        <form wire:submit.prevent="addPost">
            <div class="form-group">
                <label for="body" class="sr-only">Your post</label>
                <textarea name="body" id="body" class="form-control @error('body') is-invalid @enderror" wire:model="body"></textarea>

                @error('body')
                    <span class="invalid-feedback">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>

            <button type="submit" class="btn btn-primary">Post</button>
        </form>
    </div>
</div>
